package com.korbiak.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LongestPlateau {

    public class Plateau implements Comparable<Plateau> {
        int indexOfFirst;
        int indexOfLast;

        public Plateau(int indexOfFirst, int indexOfLast) {
            this.indexOfFirst = indexOfFirst;
            this.indexOfLast = indexOfLast;
        }

        @Override
        public String toString() {
            return "Plateau{" +
                    "indexOfFirst=" + indexOfFirst +
                    ", indexOfLast=" + indexOfLast +
                    '}';
        }

        public int getIndexOfFirst() {
            return indexOfFirst;
        }

        public void setIndexOfFirst(int indexOfFirst) {
            this.indexOfFirst = indexOfFirst;
        }

        public int getIndexOfLast() {
            return indexOfLast;
        }

        public void setIndexOfLast(int indexOfLast) {
            this.indexOfLast = indexOfLast;
        }


        @Override
        public int compareTo(Plateau o) {
            int length = this.getIndexOfLast() - this.getIndexOfFirst();
            int lengthO = o.getIndexOfLast() - o.getIndexOfFirst();
            return Integer.compare(lengthO, length);
        }
    }

    public Plateau getLongestPlateau(final int[] arr) {
        List<Plateau> plateauList = new ArrayList<>();
        //222554577
        int firstIndex = 0;
        int lastIndex = 0;
        int length = 0;
        boolean flag = false;
        for (int i = 0, k = 1; i < arr.length; i++, k++) {
            if (k != arr.length) {
                if (arr[i] == arr[k]) {
                    length++;
                    if (length == 1) {
                        firstIndex = i;
                    }
                    lastIndex = k;
                    flag = true;
                } else flag = false;
            } else flag = false;
            if (!flag && length > 0) {
                plateauList.add(new Plateau(firstIndex, lastIndex));
                firstIndex = 0;
                lastIndex = 0;
                length = 0;
            }
        }

        for (int i = 0; i < plateauList.size(); i++) {
            firstIndex = plateauList.get(i).indexOfFirst;
            lastIndex = plateauList.get(i).indexOfLast;
            if (!(firstIndex == 0 && lastIndex + 1 == arr.length)) {
                if (firstIndex == 0) {
                    if (arr[lastIndex] < arr[lastIndex + 1]) {
                        plateauList.remove(i);
                    }
                } else if (lastIndex + 1 == arr.length) {
                    if (arr[firstIndex] < arr[firstIndex - 1]) {
                        plateauList.remove(i);
                    }
                } else if (arr[firstIndex] < arr[firstIndex - 1]
                        || arr[lastIndex] < arr[lastIndex + 1]) {
                    plateauList.remove(i);
                }
            }
        }

        Collections.sort(plateauList);
        if (plateauList.isEmpty()) {
            return new Plateau(0, 0);
        }
        return plateauList.get(0);
    }

}
