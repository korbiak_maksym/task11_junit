package com.korbiak.model;

public class Model {


    public Minesweeper minesweeper;
    public LongestPlateau longestPlateau;

    public Model() {
        this.longestPlateau = new LongestPlateau();
        this.minesweeper = new Minesweeper(10, 10, 0.6);
    }

    public String getLongestPlateau(int[] arr) {
        return String.valueOf(longestPlateau.getLongestPlateau(arr));
    }

    public String getMinesweeper() {
        String answer = "";
        answer += minesweeper.getAreaString();
        minesweeper.fillCharArea();
        minesweeper.replacePeriods();
        answer += "\n";
        answer += minesweeper.getCharAreaString();

        return answer;
    }
}
