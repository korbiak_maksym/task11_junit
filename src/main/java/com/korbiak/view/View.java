package com.korbiak.view;

import com.korbiak.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input;
    private static Logger logger = LogManager.getLogger(View.class);


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", "1 - LongestPlateau");
        menu.put("2", "2 - Minesweeper");
        menu.put("Q", "Q - exit");
    }

    public View() {

        controller = new Controller();
        input = new Scanner(System.in);
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::getEx1);
        methodsMenu.put("2", this::getEx2);

    }

    private void getEx2() {
        logger.info(controller.getMinesweeper());
    }

    private void getEx1() {
        int[] arr = {2, 2, 2, 5, 5, 4, 5, 7, 7, 7};
        logger.info(controller.getLongestPlateau(arr));
    }


    public void show() {
        String keyMenu;
        do {
            outputMenu();
            logger.info("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).getCom();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }

    private void outputMenu() {
        logger.info("\nMENU:");
        for (String str : menu.values()) {
            logger.info(str);
        }
    }
}
