package com.korbiak.controller;

import com.korbiak.model.Model;

public class Controller {

    public Model model;

    public Controller() {
        this.model = new Model();
    }

    public String getLongestPlateau(int[] arr){
        return model.getLongestPlateau(arr);
    }

    public String getMinesweeper(){
        return model.getMinesweeper();
    }
}
