package com.korbiak.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.runners.Parameterized;

import java.security.acl.LastOwnerException;

public class LongestPlateauTest {


    @RepeatedTest(2)
    public void test1getLongestPlateau() {
        LongestPlateau longestPlateau = new LongestPlateau();
        LongestPlateau.Plateau plateau =
                longestPlateau.getLongestPlateau(new int[]{1, 25, 4, 5, 5, 2, 3, 2});
        Assertions.assertNotEquals(plateau.getIndexOfFirst(), 0);
        Assertions.assertNotEquals(plateau.getIndexOfLast(), 0);
    }

}
