package com.korbiak.model;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ModelTest {

    @InjectMocks
    Model model;

    @Mock
    Minesweeper minesweeper;

    @Before
    public void ModelTest() {
        model = new Model();
//        minesweeper = model.minesweeper;
    }

    @Test
    public void getMinesweeperTest() {
        String eqStr = ". * . . * . . * * . \n" +
                "* * * . * . . * . * \n" +
                ". . . . . * . * * . \n" +
                ". . * . * * . . * . \n" +
                "* . * . * . . . . . \n" +
                "* . . * . . . . . * \n" +
                "* * . . . * . * * * \n" +
                "* . . . * * . . . * \n" +
                ". . . . . . . . . . \n" +
                ". . * . . . . * . .";
        String eqStr2 = "1 1 0 2 * * * * 3 * \n" +
                "* 3 3 4 * * * 6 5 * \n" +
                "3 * * * 5 6 * * * 2 \n" +
                "* 3 4 4 * * 5 6 4 2 \n" +
                "2 1 2 * 4 5 * * * 2 \n" +
                "2 * 4 2 2 * * * 5 * \n" +
                "2 * * 4 4 4 4 2 3 * \n" +
                "2 4 * * * * 2 1 1 1 \n" +
                "* 3 2 5 * 5 * 2 1 0 \n" +
                "* 1 0 2 * 3 2 * 1 0";
        when(minesweeper.getAreaString()).thenReturn(eqStr);
        when(minesweeper.getCharAreaString()).thenReturn(eqStr2);
        assertEquals(model.getMinesweeper(), ". * . . * . . * * . \n" +
                "* * * . * . . * . * \n" +
                ". . . . . * . * * . \n" +
                ". . * . * * . . * . \n" +
                "* . * . * . . . . . \n" +
                "* . . * . . . . . * \n" +
                "* * . . . * . * * * \n" +
                "* . . . * * . . . * \n" +
                ". . . . . . . . . . \n" +
                ". . * . . . . * . . \n" +
                "\n" +
                "3 * 3 3 * 2 2 * * 2 \n" +
                "* * * 3 * 3 4 * 6 * \n" +
                "2 3 3 4 4 * 4 * * 3 \n" +
                "1 3 * 4 * * 3 3 * 2 \n" +
                "* 4 * 5 * 3 1 1 2 2 \n" +
                "* 4 3 * 3 2 2 2 4 * \n" +
                "* * 2 2 4 * 3 * * * \n" +
                "* 2 1 1 * * 3 2 4 * \n" +
                "1 1 1 2 2 2 2 1 2 1 \n" +
                "0 1 * 1 0 0 1 * 1 0");
    }
}
